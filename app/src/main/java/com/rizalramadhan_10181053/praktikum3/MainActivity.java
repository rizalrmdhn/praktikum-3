package com.rizalramadhan_10181053.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    EditText editNama, editNim;
    Button buttonSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNama = findViewById(R.id.editnama);
        editNim = findViewById(R.id.editnim);
        buttonSubmit = findViewById(R.id.btnsubmit);

        sharedPreferences = getSharedPreferences("Login", MODE_PRIVATE);
        sharedPreferences.contains("Nama");
        sharedPreferences.contains("Nim");

        buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("Nama"), editNama.getText().toString();
                editor.putString("Nim"), editNama.getText().toString();

                editor.apply();

                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });
    }
}