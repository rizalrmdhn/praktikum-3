package com.rizalramadhan_10181053.praktikum3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

public class ProfileActivity extends AppCompatActivity {

    SharedPreferences sharedPreferences;
    TextView txtnama, txtnim;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtnama = findViewById(R.id.txtnama);
        txtnim = findViewById(R.id.txtnim);

        sharedPreferences = getSharedPreferences("Login", MODE_PRIVATE);

        txtnama.setText(sharedPreferences.getString("nama", null));
        txtnim.setText(sharedPreferences.getString("nim", null));
    }
}